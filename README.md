Voice recognition service 
==============================

## Example
- sbt run

## Implemented Features

* Sign Up
* Sign In (Credentials)
* Social Auth (Facebook, Google+, VK, Twitter, Xing, Yahoo)
* Two-factor authentication with Clef
* Dependency Injection with Guice
* Publishing Events
* Avatar service
* Remember me functionality
* [Security headers](https://www.playframework.com/documentation/2.4.x/SecurityHeaders)
* [CSRF Protection](https://www.playframework.com/documentation/2.4.x/ScalaCsrf)
* play-slick database access

# License

The code is licensed under [Apache License v2.0](http://www.apache.org/licenses/LICENSE-2.0).

# Frontend repository

* [Main repository](https://gitlab.com/NightBerserk/voice-recognition)

* Based on react, redux, webpack, can be build to electron: desctop, crossplatform app

* Login Page (created);
* Main page (in progress);